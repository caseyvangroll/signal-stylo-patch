# Signal Stylo Patch

## Usage
1. Install [TamperMonkey in Chrome](https://chromewebstore.google.com/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
2. Create a new userscript with the following config:
```
// ==UserScript==
// @name         Signal Stylo Patch
// @version      2023-12-24
// @description  Add button to download Signal MIDI tracks in custom Stylophone format
// @author       Casey Van Groll
// @match        https://signal.vercel.app/edit
// @grant        none
// @require      https://gitlab.com/caseyvangroll/signal-stylo-patch/-/raw/main/patch.js
// ==/UserScript==
```
3. Navigate to https://signal.vercel.app/edit
4. Download any track in custom Stylophormat
