function mapNotes(notes) {
  const result = [];
  let lasttick = 0;

  // Trim silence at start
  if (notes[0] && notes[0].tick !== 0) {
    const silentStartLength = notes[0].tick;
    notes.forEach(note => note.tick -= silentStartLength)
  }

  notes.forEach(note => {
    // Map note value to closest valid Stylophone value
    let value = note.noteNumber - 44;
    while (value < 1) {
      value += 12;
    }
    while (value > 20) {
      value -= 12;
    }

    // If prev note overlapped this one then trim prev note
    const timediff = note.tick - lasttick;
    if (timediff < 0) {
      result[result.length - 1].duration += timediff;
    }
    // If silence between prev note and this one then insert rest
    else if (timediff > 0) {
      result.push({
        value: 'R',
        duration: timediff,
      })
    }

    lasttick = note.tick + note.duration;
    result.push({
      value,
      duration: note.duration,
    })
  });
  return result.map(({ value, duration }) => `${value}-${duration}`).join(',');
}

function convertToStyloTrack(trackIndex, midiTrack) {
    const events = [...midiTrack.events];
    const trackName = events.find(event => event.type === 'meta' && event.subtype === "trackName")?.text || `track-${trackIndex}`;
    const noteEvents = events.filter(event => event.subtype === 'note')
    return {
        filename: `${trackName}.stylo`,
        content: mapNotes(noteEvents),
    };
}
function getExistingTracks() {
    const existingTracks = [...document.querySelectorAll('.Pane.vertical.Pane1 > div > div')]
    existingTracks.pop() // Remove add track button
    return existingTracks;
}
function handleDownload(event) {
    const tracks = getExistingTracks();
    const trackIndex = tracks.findIndex(track => track.contains(event.target)) + 1;
    const agent = window.__REACT_DEVTOOLS_GLOBAL_HOOK__.reactDevtoolsAgent
    const rootStoreNodeId = 4
    const rendererId = 1
    agent.rendererInterfaces[rendererId].inspectElement(0, rootStoreNodeId, [], true)
    agent.rendererInterfaces[rendererId].storeAsGlobal(rootStoreNodeId, ['props'], 0)
    const track = window['$reactTemp0'].value.song.tracks[trackIndex];
    const { filename, content } = convertToStyloTrack(trackIndex, track);
    const link = document.createElement('a');
    link.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(content));
    link.setAttribute('download', filename);
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
function attachButtonToTrack(node) {
    const downloadBtn = document.createElement('button')
    downloadBtn.textContent = '⇩'
    downloadBtn.style.marginLeft = '1rem'
    downloadBtn.onclick = handleDownload
    node.lastChild.lastChild.appendChild(downloadBtn)
}
function attachToExistingTracks() {
    const existingTracks = getExistingTracks();
    if (existingTracks.length) {
        existingTracks.forEach(attachButtonToTrack)
    }
}
function attachTracksObserver() {
    const tracksContainer = document.querySelector('.Pane.vertical.Pane1 > div')
    if (tracksContainer) {
        attachToExistingTracks()
        const tracksObserver = new MutationObserver((mutationList) => {
            for (const mutation of mutationList) {
                if (mutation.type === "childList" && mutation.addedNodes.length) {
                    for (const node of mutation.addedNodes) {
                        attachButtonToTrack(node);
                    }
                }
            }
        });
        tracksObserver.observe(tracksContainer, { childList: true });
    }
}
function attachMainPaneObserver() {
    const mainPane = document.querySelector('#root > div > div > div > div:nth-child(2)')
    if (mainPane) {
        const mainPaneObserver = new MutationObserver((mutationList) => {
            mutationList.forEach(mutationRecord => {
                if ([...mutationRecord.addedNodes].some(node => node.matches('div.SplitPane.vertical'))) {
                    attachTracksObserver();
                }
            })
        })
        mainPaneObserver.observe(mainPane, { childList: true, subtree: true });
    }
}


console.log('Signal Stylo Patch: Waiting for devtools...')
const interval = setInterval(() => {
    if (window.__REACT_DEVTOOLS_GLOBAL_HOOK__.reactDevtoolsAgent) {
        attachMainPaneObserver()
        attachToExistingTracks()
        console.log('Signal Stylo Patch: Installed.')
        clearInterval(interval);
    }
}, 100)
